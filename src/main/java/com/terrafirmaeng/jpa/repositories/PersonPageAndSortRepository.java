package com.terrafirmaeng.jpa.repositories;

import com.terrafirmaeng.jpa.model.Person;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Lane Maxwell
 */
@Repository
public interface PersonPageAndSortRepository extends PagingAndSortingRepository<Person, Long> {

}
