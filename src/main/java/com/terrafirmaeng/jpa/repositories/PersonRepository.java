package com.terrafirmaeng.jpa.repositories;

import com.terrafirmaeng.jpa.model.Person;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Lane Maxwell
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Long>, JpaSpecificationExecutor {

    public Set<Person> findByFirstNameAndLastName(String firstName, String LastName);

    public Set<Person> findByFirstNameOrLastName(String firstName, String LastName);

    public Person findByGovernmentAssignedBarcode (Long governmentAssignedBarcode);

    public List<Person> findByDobLessThan (Date dob);

    public List<Person> findByDateOfBirth (Date dob);

    @Query("select p from Person p where p.firstName = :first_name or p.lastName = :last_name")
    public List<Person> findByLastnameOrFirstnameNamedParameter(@Param("last_name") String lastName,
                                                                @Param("first_name") String firstName);

    @Query("select p from #{#entityName} p where p.firstName = ?2 or p.lastName = ?1")
    public List<Person> findByLastnameOrFirstnameOrderedParameter(String lastName, String firstName);
}
