package com.terrafirmaeng.jpa.repositories;

import com.terrafirmaeng.jpa.model.Hobby;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Lane Maxwell
 */
@Repository
public interface HobbyRepository extends CrudRepository<Hobby, Long> {

    Hobby findByName(String name);
}
