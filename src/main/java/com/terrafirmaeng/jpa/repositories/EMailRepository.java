package com.terrafirmaeng.jpa.repositories;

import com.terrafirmaeng.jpa.model.EMail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Lane Maxwell
 */
@Repository
public interface EMailRepository extends CrudRepository<EMail, Long> {
}
