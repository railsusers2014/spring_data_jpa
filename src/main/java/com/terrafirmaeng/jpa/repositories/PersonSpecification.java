package com.terrafirmaeng.jpa.repositories;

import com.terrafirmaeng.jpa.model.Person;
import com.terrafirmaeng.jpa.model.Person_;
import org.joda.time.LocalDate;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author Lane Maxwell
 */
public class PersonSpecification {

    /**
     * This specification will match for any person who has a dob of birth < 50 years from today
     */
    public static Specification<Person> isSeniorCitizen () {
        return new Specification<Person>() {
            @Override
            public Predicate toPredicate(Root<Person> personRoot, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.lessThan(personRoot.get(Person_.dob), new LocalDate().minusYears(50).toDate());
            }
        };
    }
}
