package com.terrafirmaeng.jpa.model;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Lane Maxwell
 */
@StaticMetamodel(Person.class)
public abstract class Person_ {

    public static volatile SingularAttribute<Person, Long> id;
    public static volatile SingularAttribute<Person, String> lastName;
    public static volatile SingularAttribute<Person, Date> dob;
    public static volatile SetAttribute<Person, Hobby> hobbies;
    public static volatile SingularAttribute<Person, Long> governmentAssignedBarcode;
    public static volatile SingularAttribute<Person, String> firstName;
    public static volatile SetAttribute<Person, EMail> emailAddresses;

}

