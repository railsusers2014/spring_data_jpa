package com.terrafirmaeng.jpa.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * @author Lane Maxwell
 */
@Entity
@Table(name="Person", uniqueConstraints = {@UniqueConstraint(columnNames = {"barcode"})})
@NamedQuery(name="Person.findByDateOfBirth",
            query="select p from Person p where p.dob = ?1 ")
public class Person {

    @Id
    @GeneratedValue
    @Column(name="person_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER)
    private Set<EMail> emailAddresses;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "person_hobbies",
            joinColumns = {@JoinColumn(name = "person_id", nullable = false, updatable = false) },
            inverseJoinColumns = {@JoinColumn(name = "hobby_id", nullable = false, updatable = false) })
    private Set<Hobby> hobbies;

    @Column(name="barcode")
    private Long governmentAssignedBarcode;

    @Column(name="birthday")
    private Date dob;

    @Version
    private Long version;

    public Person() {
    }

    public Person(String firstName, String lastName, Long governmentAssignedBarcode, Date dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.governmentAssignedBarcode = governmentAssignedBarcode;
        this.dob = dob;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<EMail> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(Set<EMail> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public Set<Hobby> getHobbies() {
        return hobbies;
    }

    public void setHobbies(Set<Hobby> hobbies) {
        this.hobbies = hobbies;
    }

    public Long getGovernmentAssignedBarcode() {
        return governmentAssignedBarcode;
    }

    public void setGovernmentAssignedBarcode(Long governmentAssignedBarcode) {
        this.governmentAssignedBarcode = governmentAssignedBarcode;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!id.equals(person.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (id == null) ? 0 : id.hashCode();
    }
}
