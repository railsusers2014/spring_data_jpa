package com.terrafirmaeng.jpa.model;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Lane Maxwell
 */
@Entity
@Table(name="Hobby", uniqueConstraints = {@UniqueConstraint(name="hobby_name_constraint", columnNames = {"name"})})
public class Hobby {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "hobbies")
    private Set<Person> persons;

    @Column(name="name")
    private String name;

    public Hobby () {

    }

    public Hobby (String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
