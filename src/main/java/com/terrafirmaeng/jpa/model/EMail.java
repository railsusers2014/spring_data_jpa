package com.terrafirmaeng.jpa.model;

import javax.persistence.*;

/**
 * @author Lane Maxwell
 */
@Entity
@Table (name="email_address")
public class EMail {

    @Id
    @GeneratedValue
    @Column(name="email_id")
    private Long id;

    @Column(name="email")
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id", nullable = false)
    private Person person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}