package com.terrafirmaeng.jpa.persistence;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;

import java.io.Serializable;

/**
 * @author Lane Maxwell
 */
public class DirtyInterceptor extends EmptyInterceptor {

    public boolean onFlushDirty(java.lang.Object entity, java.io.Serializable id, java.lang.Object[] currentState, java.lang.Object[] previousState, java.lang.String[] propertyNames, org.hibernate.type.Type[] types) {
        System.out.println("the entity is " + entity);
        return false;
    }

    @Override
    public void onCollectionUpdate(Object collection, Serializable key) throws CallbackException {
        System.out.println(collection);
    }

}
