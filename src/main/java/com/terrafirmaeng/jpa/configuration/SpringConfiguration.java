package com.terrafirmaeng.jpa.configuration;

import com.terrafirmaeng.jpa.configuration.environments.Dev;
import com.terrafirmaeng.jpa.configuration.environments.Mem;
import com.terrafirmaeng.jpa.configuration.environments.Prod;
import com.terrafirmaeng.jpa.persistence.DirtyInterceptor;
import org.hibernate.Interceptor;
import org.hibernate.ejb.HibernatePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import static org.hibernate.cfg.AvailableSettings.*;
import static org.hibernate.ejb.AvailableSettings.*;


import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories (basePackages = "com.terrafirmaeng.jpa.repositories")
@EnableTransactionManagement
@PropertySources(value={@PropertySource("classpath:hibernate.properties")})
@Import({Dev.class, Prod.class, Mem.class})
public class SpringConfiguration {
    @Resource
    private Environment environment;

    @Bean
    @Autowired
    @Qualifier("entityManagerFactory")
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);

        return transactionManager;
    }

    @Bean
    public Interceptor hibernateInterceptor () {
        return new DirtyInterceptor();
    }

    @Bean(name = "entityManagerFactory")
    @Autowired(required = true)
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource, Interceptor interceptor) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPackagesToScan(environment.getRequiredProperty("packages.to.scan"));
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistence.class);

        Properties hibernateProperties = new Properties();
        hibernateProperties.put(DIALECT, environment.getRequiredProperty(DIALECT));
        hibernateProperties.put(HBM2DDL_AUTO, environment.getRequiredProperty(HBM2DDL_AUTO));
        hibernateProperties.put(NAMING_STRATEGY, environment.getRequiredProperty(NAMING_STRATEGY));
        hibernateProperties.put(SHOW_SQL, environment.getRequiredProperty(SHOW_SQL));
        hibernateProperties.put(USE_SECOND_LEVEL_CACHE, environment.getRequiredProperty(USE_SECOND_LEVEL_CACHE));
        hibernateProperties.put(INTERCEPTOR, interceptor);

        entityManagerFactoryBean.setJpaProperties(hibernateProperties);
        return entityManagerFactoryBean;
    }
}