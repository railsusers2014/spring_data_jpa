package com.terrafirmaeng.jpa.persistence;
/* 
 *  FOR INTERNAL USE ONLY. NOT A CONTRIBUTION.
 *
 *  This software source code contains valuable, confidential, trade secret
 *  information owned by Enterprise Rent-A-Car Company and is protected by
 *  copyright laws and international copyright treaties, as well as other
 *  intellectual property laws and treaties.
 *
 *  ACCESS TO AND USE OF THIS SOURCE CODE IS RESTRICTED TO AUTHORIZED PERSONS
 *  WHO HAVE ENTERED INTO CONFIDENTIALITY AGREEMENTS WITH ENTERPRISE RENT-A-CAR
 *  COMPANY.
 *
 *  This source code may not be licensed, disclosed or used except as authorized
 *  in writing by a duly authorized officer of Enterprise Rent-A-Car Company.
 *
 */

import com.terrafirmaeng.jpa.configuration.SpringConfiguration;
import com.terrafirmaeng.jpa.model.EMail;
import com.terrafirmaeng.jpa.model.Hobby;
import com.terrafirmaeng.jpa.model.Person;
import com.terrafirmaeng.jpa.model.Person_;
import com.terrafirmaeng.jpa.repositories.EMailRepository;
import com.terrafirmaeng.jpa.repositories.HobbyRepository;
import com.terrafirmaeng.jpa.repositories.PersonPageAndSortRepository;
import com.terrafirmaeng.jpa.repositories.PersonRepository;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;
import static org.springframework.data.domain.Sort.*;

import static com.terrafirmaeng.jpa.repositories.PersonSpecification.*;

import static org.junit.Assert.*;

/**
 * @author Lane Maxwell
 */
@RunWith (SpringJUnit4ClassRunner.class)
@ContextConfiguration (classes = {SpringConfiguration.class}, loader=AnnotationConfigContextLoader.class)
@ActiveProfiles("mem")
public class PersistenceTest {
    public static Date getRandomDate() {
        Integer randomDays = new Double(Math.random() * 30).intValue();
        Integer randomMonths = new Double(Math.random() * 12).intValue();
        Integer randomYears = new Double(Math.random() * 95).intValue();

        return new LocalDate().minusDays(randomDays)
                .minusYears(randomYears)
                .minusMonths(randomMonths).toDate();
    }

    // array of peeps with random birthdates
    public Person [] persons = {
            new Person("Lane", "Maxwell", 1L, getRandomDate()),
            new Person("Tom", "Smith", 2L, getRandomDate()),
            new Person("John", "Smith", 3L, getRandomDate()),
            // there's nothing saying we can't have two identical names
            new Person("John", "Smith", 4L, getRandomDate()),
            new Person("Karl", "Smith", 5L, getRandomDate()),
            new Person("John", "Walker", 6L, getRandomDate()),
            new Person("Tom", "Collins", 7L, getRandomDate()),
            new Person("Tom", "Thompson", 8L, getRandomDate()),
            new Person("Tom", "AndJerry", 9L, getRandomDate()),
            new Person("James", "Beam", 10L, getRandomDate()),
            new Person("Randall", "Overdaplace", 11L, getRandomDate()),
            new Person("Benjamin", "Franklin", 12L, new DateTime(1706, 1, 17, 17, 21).toDate())
    };
    public String [] hobbies = {"Kite Flying", "Painting", "Drag Racing", "Day Dreaming", "Doodling", "Vintage Libgloss Collecting", "Amateur Gastroenterologist",
                                "Fly Fishing", "Lion Taming", "Pick Pocketing", "Watermelon Connoisseur", "Graffiti Artist", "Whitewater Rafting (In Hawaiian Swimming Trunks)"};

    @Autowired PersonRepository personRepository;

    @Autowired
    PersonPageAndSortRepository personPageAndSortRepository;

    @Autowired EMailRepository eMailRepository;

    @Autowired HobbyRepository hobbyRepository;

    @PersistenceContext EntityManager entityManager;

    static boolean dataConstructed;

    @Before
    // setup some test data, one time only
    public void setupData () {
        if (!dataConstructed) {
            for (String hobbyName : hobbies) {
                hobbyRepository.save(new Hobby(hobbyName));
            }

            for (Person person : persons) {
                personRepository.save(person);
            }

            dataConstructed = true;
        }
    }

    @Test
    public void persistAPerson() {
        // Find the person by using one of the find methods defined on the interface
        Set<Person> persons = personRepository.findByFirstNameAndLastName("Lane", "Maxwell");

        // There should only be a single person in our database mathing the criteria.
        assertEquals(persons.size(), 1);
        Person person = persons.iterator().next();

        // Create an email address, and attach it to the person
        EMail eMail = new EMail();
        eMail.setEmail("lane_maxwell@terrafirmaeng.com");
        eMail.setPerson(person);
        eMailRepository.save(eMail);

        // After persisting the email, let's rehydrate the person object
        person = personRepository.findOne(person.getId());
        assertTrue(person.getEmailAddresses().size() == 1);
    }

    /**
     * One of our persons in our static people is Benjamin Franklin
     * let's add a hobby to our founding father, and while we're at it
     * let's test our {@link javax.persistence.criteria.Predicate}
     * and see if he qualifies as a senior citizen
     *
     * @see javax.persistence.criteria.Predicate
     */
    @Test
    public void testPredication() {
        List<Person> personsWhoAreSeniorCitizens = personRepository.findAll(isSeniorCitizen());
        Person personWhoHappensToBeBenjaminFranklin = personRepository.findByFirstNameAndLastName("Benjamin","Franklin").iterator().next();
        // Ben Franklin is indeed a senior citizen
        assertTrue(personsWhoAreSeniorCitizens.contains(personWhoHappensToBeBenjaminFranklin));

        // Let's add one hobby to Ben
        personWhoHappensToBeBenjaminFranklin.getHobbies().add(hobbyRepository.findByName("Kite Flying"));
        personRepository.save(personWhoHappensToBeBenjaminFranklin);

        // At this point, the hobby should be persisted to the DB, if I get a copy of BF's person object, it should have the hobby on it
        personWhoHappensToBeBenjaminFranklin = personRepository.findByFirstNameAndLastName("Benjamin","Franklin").iterator().next();
        assertTrue(personWhoHappensToBeBenjaminFranklin.getHobbies().size() == 1);
    }

    /**
     * You aren't forced into using the repositories.  Simply add a
     * @PersistenceContext EntityManager to your class and you can have direct
     * access to the JPA entity manager.
     */
    @Test
    public void testEntityManaagerUsage () {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
        Root<Person> personRoot = criteriaQuery.from(Person.class);

        criteriaQuery.where(criteriaBuilder.lessThan(personRoot.get(Person_.dob), new LocalDate().minusYears(50).toDate()));
        TypedQuery<Person> typedQuery = entityManager.createQuery(criteriaQuery);
        List<Person> persons = typedQuery.getResultList();

        assertNotNull(persons);
    }

    @Test
    public void testFindSeniorCitizen () {
        List<Person> seniorCitizens = personRepository.findByDobLessThan(new LocalDate().minusYears(50).toDate());
        assertTrue(seniorCitizens.size() >= 1);
    }

    @Test
    public void testNamedQueryFind () {
        List<Person> persons = personRepository.findByDateOfBirth (new DateTime(1706, 1, 17, 17, 21).toDate());
        assertTrue (persons.size() >= 1);
    }

    @Test
     public void testNamedParameterFind () {
        List<Person> persons = personRepository.findByLastnameOrFirstnameNamedParameter("Maxwell", "Lane");
        assertTrue(persons.size() == 1);
    }

    @Test
    public void testOrderedParameterFind () {
        List<Person> persons = personRepository.findByLastnameOrFirstnameOrderedParameter("Maxwell", "Lane");
        assertTrue(persons.size() == 1);
    }

    @Test
    public void testPaging() {
        Page<Person> pageOfPeople = personPageAndSortRepository.findAll(new PageRequest(0, 5));
        assertEquals(pageOfPeople.getSize(), 5);

        Iterable<Person> personIterable = personPageAndSortRepository.findAll(
                new Sort(new Order(Direction.DESC, Person_.lastName.getName()),
                new Order(Direction.ASC, Person_.firstName.getName())));
        Iterator<Person> personIterator = personIterable.iterator();
        Person person = personIterator.next();
        // "Walker" should be at the top of the list, sorted by last name descending
        assertEquals(personRepository.findByGovernmentAssignedBarcode(6L), person);


        PageRequest pageRequest = new PageRequest(0, 100,
                new Sort(Direction.DESC, Person_.lastName.getName()).and(
                new Sort(Direction.ASC, Person_.firstName.getName())));

        Page<Person> sortedPersonPage = personPageAndSortRepository.findAll(pageRequest);
        sortedPersonPage.getContent().get(0);
        // "Walker" should be at the top of the list, sorted by last name descending
        assertEquals(personRepository.findByGovernmentAssignedBarcode(6L), person);
    }
}