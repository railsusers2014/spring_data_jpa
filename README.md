
Overview
---
This application will serve as a demonstration of the capabilities of JPA, and Spring Data JPA.  Please take some
time to familiarize yourself with the project structure and classes.

* All Spring configuration classes are located in configuration.. package
* All Entities are in the model package
* The repositories package are where the Spring Repositories and Specifications are kept
* There is a single unit test that demonstrates all the capabilities outlined in the presentation


Tasks
---
* Identify the JPA provider used in this project
* The Person entity has mappings to external (Email, Hobby) classes.  How could you modify one of these mappings
  so that when the person is deleted, associated Email addresses are also deleted?
* The application demonstrates OneToMany and ManyToOne mappings.  Think of something is unique to a Person, and add
  a new entity to capture this.  Make the new entity a OneToOne mapping to Person.
* Add a test case to the PersistenceTest class to verify that you can persist the new entity, associate it to a person
  and retrieve the person with the new entity mapped (look to other unit tests for guidance).